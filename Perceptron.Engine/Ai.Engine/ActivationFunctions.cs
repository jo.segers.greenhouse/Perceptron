﻿namespace Ai.Engine; 

public delegate double ActivationFunction(double input);

public static class ActivationFunctions {
  public static readonly ActivationFunction AlwaysZero = _ => 0;
  public static readonly ActivationFunction Step = input => input > 0.5 ? 1 : 0;
  public static readonly ActivationFunction Sigmoid = input => 1 / (1 + Math.Pow(Math.E, -input));
}