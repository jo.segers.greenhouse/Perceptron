﻿namespace Ai.Engine; 

public class Sample {
  public double[] Inputs { get; set; }
  public double Target { get; set; }
  public double Preciction { get; set; }
  public double Loss { get; set; }
}