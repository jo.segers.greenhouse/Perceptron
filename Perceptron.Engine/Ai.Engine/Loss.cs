﻿namespace Ai.Engine; 

public static class Loss {
  public static double AverageLoss(IEnumerable<double> loss) => loss.Average();
}