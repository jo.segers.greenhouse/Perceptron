﻿namespace Ai.Engine; 

public partial class Perceptron {
  public static PerceptronBuilder Create() => new();
  
  public class PerceptronBuilder {
    private readonly Perceptron _Perceptron = new();
    
    public PerceptronBuilder WithWeights(params double[] weights) {
      _Perceptron.Weights = weights;
      return this;
    }

    public PerceptronBuilder WithBias(double bias) {
      _Perceptron.Bias = bias;
      return this;
    }

    public PerceptronBuilder WithActivationFunction(ActivationFunction activationFunction) {
      _Perceptron._ActivationFunction = activationFunction;
      return this;
    }

    public Perceptron Build() {
      //TODO: Add validation
      return _Perceptron;
    }
  }
}