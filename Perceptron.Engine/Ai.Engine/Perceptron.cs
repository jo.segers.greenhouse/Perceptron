﻿namespace Ai.Engine;

public partial class Perceptron {
  private Perceptron() {}
  
  public double[] Weights { get; set; }= Array.Empty<double>();
  public double Bias { get; set; }
  private ActivationFunction _ActivationFunction = ActivationFunctions.AlwaysZero;

  public Prediction Predict(double[] inputs) {
    var weightedSum = inputs.Select((t, i) => t * Weights[i]).Sum() + Bias;

    return new() {
      WeightedSum = weightedSum,
      Result = _ActivationFunction(weightedSum),
    };
  }

  public class Prediction {
    public double WeightedSum { get; set; }
    public double Result { get; set; }
  }
}