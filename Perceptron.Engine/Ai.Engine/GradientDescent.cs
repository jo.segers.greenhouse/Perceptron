﻿namespace Ai.Engine; 

public static class GradientDescent {
  private const double _LEARNING_RATE = 0.01;
  
  public static double[] CalculateNewWeights(double[] weights, Sample sample) {
    var newWeights = new double[weights.Length];
    for (var i = 0; i < weights.Length; i++) {
      newWeights[i] = weights[i] + _LEARNING_RATE * (sample.Target - sample.Preciction) * sample.Inputs[i];
    }
    return newWeights;
  }

  public static double CalculateNewBias(double bias, Sample sample) {
    return bias + _LEARNING_RATE * (sample.Target - sample.Preciction);
  }
}