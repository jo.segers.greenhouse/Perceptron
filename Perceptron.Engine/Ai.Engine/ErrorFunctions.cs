﻿namespace Ai.Engine;

public delegate double ErrorFunction(double target, double weightedSum); 

/// <summary>
/// An 'Error Function' (also called 'Loss Function' or 'Cost' is used to calculate the distance between a given prediction and the target.
/// </summary>
public static class ErrorFunctions {
  public static readonly ErrorFunction CrossEntropyLoss = (target, weightedSum) => {
    var part1 = target * Math.Log10(weightedSum);
    var part2 = (1 - target) * Math.Log10(1 - weightedSum);
    return -(part1 + part2);
  };
}