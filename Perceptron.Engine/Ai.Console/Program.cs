﻿using Ai.Engine;

Console.WriteLine("Perceiving the Perceptron");

var samples = new Sample[] {
  new() { Inputs = new[]{ 0.1, 0.5, 0.2 }, Target = 1 },
  new() { Inputs = new[]{ 0.2, 0.3, 0.1 }, Target = 0 },
  new() { Inputs = new[]{ 0.7, 0.4, 0.2 }, Target = 1 },
  new() { Inputs = new[]{ 0.1, 0.4, 0.3 }, Target = 0 },
};

var perceptron = Perceptron.Create()
                           .WithWeights(0.4, 0.2, 0.6)
                           .WithBias(0.5)
                           .WithActivationFunction(ActivationFunctions.Sigmoid)
                           .Build();

for (var epoch = 0; epoch < 100; epoch++) {
  foreach (var sample in samples) {
    var prediction = perceptron.Predict(sample.Inputs);

    sample.Preciction = prediction.Result;
    sample.Loss = ErrorFunctions.CrossEntropyLoss(sample.Target, prediction.WeightedSum);

    // learn stuff: calculate new weights and bias
    perceptron.Weights = GradientDescent.CalculateNewWeights(perceptron.Weights, sample);
    perceptron.Bias = GradientDescent.CalculateNewBias(perceptron.Bias, sample);
  }

  var averageLoss = Loss.AverageLoss(samples.Select(x => x.Loss));

  Console.WriteLine($"epoch {epoch}, average loss: {averageLoss}");
}

Console.WriteLine("in[0]\tin[1]\tin[2]\ty\t^y\tloss");
foreach (var sample in samples) {
  Console.WriteLine($"{sample.Inputs[0]}\t{sample.Inputs[1]}\t{sample.Inputs[2]}\t{sample.Target}\t{sample.Preciction}\t{sample.Loss}");
}
Console.WriteLine($"Perceptron config:\n\tweights: {string.Join(", ",perceptron.Weights)}\n\tbias: {perceptron.Bias}");



